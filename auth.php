<?php
session_start();
session_name("admin");

class Auth
{
  private $login;
  private $passwd;

  public function __construct()
  {
    $this->login = 'p4sh';
    $this->passwd = '123';
  }

  public function showAuth()
  {
    echo '  <!DOCTYPE html>
    <html lang="en">
    <head>
      <meta charset="utf-8">
      <title>Пожалуйста, авторизуйтесь</title>
      <meta name="description" content="">
      <meta name="author" content="">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <link href="//fonts.googleapis.com/css?family=Roboto:400,300,600" rel="stylesheet" type="text/css">
      <link rel="stylesheet" href="css/normalize.css">
      <link rel="stylesheet" href="css/skeleton.css">
      <link rel="icon" type="image/png" href="images/favicon.png">
    </head>
    <body>
      <div class="container">
         <form method="POST" >
            <div class="row">
            <div class="six columns">
              <label for="loginInp">Логин</label>
              <input class="u-full-width" placeholder="Ваш Логин" id="loginInp" name="loginInp" type="text">
            </div>
              <div class="six columns">
              <label for="passwordInp">Пароль</label>
              <input class="u-full-width" placeholder="Ваш пароль" id="passwordInp" name="passwordInp" type="password">
            </div>
              <input name="submitIn" class="button-primary" value="Принять" type="submit">
            </form>
      </div>

    </body>
    </html>';
  }


  public function authorize()
  {
    $_SESSION['adm'] = 1;
  }



  public function checkData($flogin,$fpasswd)
  {
    //пытаемся ввести логин и пароль
    if (  ($this->login == $flogin) AND ($this->passwd == $fpasswd) )
    {
        $this->authorize(); 
    }
    else {
        $this->showAuth();
    }
  }
}
/////////////////////////////////////////////////////////////////////////////
//END OF CLASS///////////////////////////////////////////////////////////////


$adm = new auth();
if (isset($_POST['submitIn']) )
{
  $adm->checkData($_POST['loginInp'],$_POST['passwordInp']);
}
elseif ($_SESSION['adm'] == 0) {
    $adm->showAuth();
}




?>
