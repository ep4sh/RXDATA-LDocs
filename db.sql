-- --------------------------------------------------------
-- Хост:                         127.0.0.1
-- Версия сервера:               5.5.48 - MySQL Community Server (GPL)
-- ОС Сервера:                   Win32
-- HeidiSQL Версия:              9.3.0.4984
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Дамп структуры базы данных ldocs
DROP DATABASE IF EXISTS `ldocs`;
CREATE DATABASE IF NOT EXISTS `ldocs` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `ldocs`;


-- Дамп структуры для таблица ldocs.users
DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `lastName` tinytext NOT NULL,
  `firstName` tinytext NOT NULL,
  `birthDate` tinytext NOT NULL,
  `rusPassport` tinytext NOT NULL,
  `interPassport` tinytext NOT NULL,
  `mobile` tinytext NOT NULL,
  `eMail` tinytext NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8;

-- Дамп данных таблицы ldocs.users: ~3 rows (приблизительно)
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` (`id`, `lastName`, `firstName`, `birthDate`, `rusPassport`, `interPassport`, `mobile`, `eMail`) VALUES
	(1, 'Горчаков', 'Рубий Долманович', '09.11.1989', 'ОУФМС РОИСИ ВАДАЛР ОБЛ', '44 123231', '+7 9999 999912', 'vlgpash@yandex.ru'),
	(2, 'Лохачёва', 'Варшава Будёновна', '03.07.1987', 'РОИСЯ ОУФМС КРАСНОДОНСКИ КРАЙ', '331 1334', '8 800 9900', 'memail@sobaka.ru'),
	(10, 'Тельцов', 'Максим Сергеевич', '01.03.1990', '1809 410944', '№ 4441144', '+7 899941 4343 443', 'max@lox.ru'),
	(14, 'Тельцов', 'Максим Сергеевич', '01.03.1990', '1809 410944', '№ 4441144', '+7 899941 4343 443', 'max@lox.ru');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
