<?php
session_start();
session_name("admin");

//require_once "auth.php";


if ($_SESSION['adm']== 1)
{
    require_once "pdo.php";
    require_once "user.php";
    echo '  

<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <title>LDOC for Volgograd Sputnik Travel Company</title>
  <meta name="description" content="">
  <meta name="author" content="">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link href="//fonts.googleapis.com/css?family=Roboto:400,300,600" rel="stylesheet" type="text/css">
  <link rel="stylesheet" href="css/normalize.css">
  <link rel="stylesheet" href="css/skeleton.css">
  <link rel="icon" type="image/png" href="images/favicon.png">
</head>
<body>
<div class="menu">
                <p> <a href="index.php"> <img src="images/home.png"> </a> </p>
	</div>
  <div class="container">
	   <form method="POST">
			  <div class="row">
				<div class="six columns">
				  <label for="LastNameInput">Фамилия</label>
				  <input class="u-full-width" placeholder="Иванов" id="LastNameInput" name="lastNameInput" type="text">
				</div>
			    <div class="six columns">
				  <label for="FirstNameInput">Имя Отчетсво</label>
				  <input class="u-full-width" placeholder="Иван Иванович" id="FirstNameInput" name="firstNameInput" type="text">
				</div>
				<div class="twelve columns">
				  <label for="BirthDateInput">Дата Рождения</label>
				  <input class="u-full-width" placeholder="01.01.1970" id="BirthDateInput" name="birthDateInput" type="text">
				</div>
				<div class="twelve columns">
				  <label for="RUSPassportInput">Паспорт РФ</label>
				  <input class="u-full-width" placeholder="1808 431398, ОУФМС РОССИИ ПО ВОЛГОГРАДСКОЙ ОБЛ. В КРАСНОАРМЕЙСКОЙ Р-НЕ ГОР. ВОЛГОГРАДА" id="RUSPassportInput" name="rusPassportInput" type="text">
				</div>
				<div class="twelve columns">
				  <label for="InterPassportInput">Заграничный паспорт</label>
				  <input class="u-full-width" placeholder="P, RUS, 51№90844134" id="interPassportInput" name="interPassportInput" type="text">
				</div>

				<div class="twelve columns">
				  <label for="TelInput">Контактный телефон</label>
				  <input class="u-full-width" placeholder="+7 903 313 98 70" id="TelInput"  name="mobileInput" type="text">

				  <label for="EmailInput">Электропочта</label>
				  <input class="u-full-width" placeholder="root@abuse.net" id="EmailInput" type="email" name="eMailInput">
				</div>
					<input class="button-primary" value="Принять" type="submit" name="addUser">
        </form>
  </div>
</body>
</html>
';

}


if (isset($_POST['addUser']))
{
    $newuser = new user();
    $newuser->setData($_POST['lastNameInput'],$_POST['firstNameInput'],$_POST['birthDateInput'],$_POST['rusPassportInput'],$_POST['interPassportInput'],$_POST['mobileInput'],$_POST['eMailInput']);
    $newuser->insData();
    
}
 ?>





